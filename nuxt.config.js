import colors from 'vuetify/es5/util/colors'

export default {
    mode: 'universal',
    // mode: 'spa',
  /*
  ** Headers of the page
  */
 // generate: {
 //    routes: [
 //      '/programs/Certificate-in-Montessori-Studies',
 //      '/programs/Certificate-in-Pre-&-Primary-Education',
 //      '/programs/Certificate-in-Early-Childhood-Education-&-Care',
 //      '/programs/Certificate-in-Nursury-Teacher-Training',
 //      '/programs/Graduate-Diploma-in-Montessori-Teacher-Training',
 //      '/programs/Graduate-Diploma-in-Pre-&-Primary-Education',
 //      '/programs/Graduate-Diploma-in-Early-Childhood-Education-&-Care',
 //      '/programs/Graduate-Diploma-in-Nursery-Teacher-Training',
 //      '/programs/Post-Graduate-Diploma-in-Montessori-Teacher-Training',
 //      '/programs/Post-Graduate-Diploma-in-Pre-&-Primary-Education',
 //      '/programs/Post-Graduate-Diploma-in-Early-Childhood-Education-&-Care',
 //      '/programs/Post-Graduate-Diploma-in-Nursery-Teacher-Training',
 //      '/programs/Post-Graduate-Diploma-in-School-Organization-Administration-and-Management',
 //    ]
 //  },
  router:{
    base:'/',
    routeNameSplitter: '/'
  },
  head: {
    titleTemplate: '%s',
    // titleTemplate: '%s - ' + process.env.npm_package_name,
    title: 'Atheneum Global Teacher Training College',
    // title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      // { hid: 'description', name: 'description', content: 'International Recognition for our teacher training courses. Enroll today for the perfect way to start your teaching career with a dream teacher’s job.' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      // { rel: 'stylesheet' , href:'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'  },
      // { rel: "stylesheet", href: "https://fonts.googleapis.com/icon?family=Material+Icons" },
      // { rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Noto+Serif|Oswald&display=swap" },
      // { rel: "stylesheet", href: "https://maxst.icons8.com/vue-static/landings/line-awesome/font-awesome-line-awesome/css/all.min.css" }

    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/scss/main.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/vue-carousel.js', ssr:false},
    { src: '~plugins/ga.js', mode: 'client' }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios'
  ],
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/scss/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: '#9c2e38',
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          primary: '#9c2e38',
          secondary: '#b0bec5',
          accent: '#8c9eff',
          error: '#b71c1c',
        },
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
